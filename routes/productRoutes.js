const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// create product (admin only)
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	productController.addProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
});

// retrieve all products
router.get("/allProducts", auth.verify, (req, res) => {
	const data = {
		userId : req.params.userId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.getAllProducts(data).then(resultFromController => res.send(resultFromController));
});

// retrieve all active products
router.get("/products", (req, res) => {
	productController.getAllActive().then(resultFromController => 
		res.send(resultFromController))
	});

// retrieve single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId)
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Update Product Information (Admin Only)
router.put("/update/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	productController.updateProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))

});


// Archive Product(Admin Only)
router.put("/archive/:productId", auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))

});

// Archive Product(Admin Only)
router.put("/active/:productId", auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.activeProduct(data).then(resultFromController => res.send(resultFromController))

});


module.exports = router; 
