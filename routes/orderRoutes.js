const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');


// Non-admin User checkout(Create Order)

router.post("/createOrder", auth.verify, (req, res) => {

const userData = auth.decode(req.headers.authorization)

  	orderController.createOrder(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
});


 

// stretch goal

// retrieve authenticated user's order

router.get("/myOrders/:userId", auth.verify, (req, res) => {

	
const data = {
		userId : req.params.userId,
		orderId : req.params.orderId
	}
		
orderController.getMyOrders(data).then(resultFromController => res.send(resultFromController))
});



module.exports = router;