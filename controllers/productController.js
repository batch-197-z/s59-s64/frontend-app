const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');

// create product (admin only) function
module.exports.addProduct = (reqBody, userData) => {

     return User.findById(userData.userId).then(result => {
        if (userData.isAdmin == false) {
            return false
        } else {

            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                count: reqBody.count,
                price: reqBody.price
            });

            return newProduct.save().then((product, error) => {

                if (error) {
                    return false

                } else {
                    return true
                }
            })
        } 
    })
};

// retrieve all products function (admin only)
module.exports.getAllProducts = (data) => {
    return Product.find(data).then((result, error) => {
        if (data.isAdmin == false) {
        return false
    } else {
        console.log(result)
        result.name = "";
        
        return result

        }
    })
}

// retrieve all active products function
module.exports.getAllActive = () => {
    return Product.find({ isActive: true }).then(result => {
        return result
    })
};

// retrieve single product function

module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result
    })
};


// Update Product Information (Admin Only) function

module.exports.updateProduct = (reqBody, userData) => {

    return Product.findById(userData.productId).then((result, error) => {

    if(userData.isAdmin === false) {
        return false
    } else {
        let updatedProduct = new Product({
            name: reqBody.name,
            description: reqBody.description,
            quantity: reqBody.quantity,
            price: reqBody.price
        });


        return updatedProduct.save().then((product, error) => {

                if(error) {

                    return false;

                } else {

                    return true
                }
            })

        } 

    })
};


// Archive Product(Admin Only) function
module.exports.archiveProduct = (data) => {

    return Product.findById(data.productId).then((result, error) => {

    if(data.isAdmin === true) {

       result.isActive = false;

            return result.save().then((archivedProduct, error) => {

                if(error) {

                    return false;

                } else {

                    return true
                }
            })

        } else {
            return false
        }
    })
};

// Active Product(Admin Only) function
module.exports.activeProduct = (data) => {

    return Product.findById(data.productId).then((result, error) => {

    if(data.isAdmin === true) {

       result.isActive = true;

            return result.save().then((activatedProduct, error) => {

                if(error) {

                    return false;

                } else {

                    return true
                }
            })

        } else {
            return false
        }
    })
};