const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

    
        userId: {
            type: String,
            required: [true, "userID is required"]
        },
        productId: {
            type: String,
            required: [true, "productId is required"]
        },
        price: {
            type: Number,
            required: [true, "price is required"]
        },
        count: {
            type: Number,
            required: [true, "count is required"]
        },
        purchasedOn: {
            type: Date,
            default: new Date()
        } 
         
    
});

module.exports = mongoose.model('Order', orderSchema)


