const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.checkEmailExists = (reqBody) => {

    // The result is sent back to the frontend via the "then" method found in the route file
    return User.find({email: reqBody.email}).then(result => {

        // The "find" method returns a record if a match is found
        if (result.length > 0) {
            return true

        // No duplicate email found
        // The user is not yet registered in the database
        } else {
            return false
        }

    })

}


// // User Registration function
module.exports.registerUser = (reqBody) => {
    
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

// Saves the created object to our database
    return newUser.save().then((user, error) => {

        // User registration failed
        if(error) {
            return false

        // User registration successful
        } else {
            return true
        }

    })
};

// User login function
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if (isPasswordCorrect) {
                return { access: auth.createAccessToken(result) }
            } else {
                return false
            }
        }
    })
};


// user authentication function
module.exports.getProfile = (userData) => {
    return User.findById(userData.id).then(result => {
        console.log(userData)
        if (result == null) {
            return false
        } else {
            console.log(result)
            result.password = "**********";
            return result;
        }
    })
};

// Retrieve All User Details (admin only)

module.exports.getAllUsers = (data) => {
    return User.find(data).then(result => {
            console.log(result)
            result.password = "**********";
            return result;
        })
    }



// set user as admin(admin only)
module.exports.setUserAsAdmin = (data) => {

    return User.findById(data.userId).then((result, error) => {

        if (data.isAdmin === true) {
        
            result.isAdmin = true;

            return result.save().then((setUserAsAdmin, error) => {

                if (error) {

                    return false;

                } else {

                    return true
                }
            })
        } else {
            return false
        }
    })
};


// set user as admin(admin only)
module.exports.setUserAsNotAdmin = (data) => {

    return User.findById(data.userId).then((result, error) => {

        if (data.isAdmin === true) {
            result.isAdmin = false;

            return result.save().then((setUserAsNotAdmin, error) => {

                if (error) {

                    return false;

                } else {

                    return true
                }
            })
        } else {
            return false
        }
    })
};

// retrieve authenticated user's order

/*module.exports.getMyOrder = (orderData, userData) => {
    return Order.findById(reqParams.userId).then(result => {
        return result
    })
};*/


